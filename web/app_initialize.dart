library jb_image_import.app_init;

import 'package:angular/application_factory.dart';
import 'package:angular/angular.dart';
import 'package:logging/logging.dart';
import 'package:logging_handlers/logging_handlers_shared.dart';
import 'dart:html';

//import own component
import 'package:jb_image_import/jb_image_import.dart';

final Logger _libLogger = new Logger("jb_image_import");

void main() {
  //init logging
  hierarchicalLoggingEnabled = true;
  Logger.root.onRecord.listen(new LogPrintHandler());

  Logger.root.level = Level.INFO;
  _libLogger.level = Level.ALL;

  applicationFactory().addModule(new App()).rootContextType(TempRootContext).run();
}

class App extends Module {

  App() {
    install(new JbImageImportModule());
  }

}

@Injectable()
class TempRootContext {
  File file;

  String dataUrl;

}