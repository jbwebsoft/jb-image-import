library jb_image_import.helper;

import 'dart:async';
import 'dart:html';

Future<String> getImageFromFileAsDataUrl(File file) {
  var completer = new Completer<String>();
  var reader = new FileReader();

  reader.onLoadEnd.listen((ProgressEvent e) {
    completer.complete(reader.result as String);
  });

  reader.readAsDataUrl(file);
  return completer.future;
}