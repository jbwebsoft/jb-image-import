library jb_image_import.component;

import 'dart:html';

import 'package:angular/angular.dart';
import 'package:jb_image_import/src/helper.dart';
import 'package:logging/logging.dart';

@Component(
    selector: "jb-image-import",
    templateUrl: "packages/jb_image_import/src/component_template.html",
    useShadowDom: true)
class JbImageImport implements ShadowRootAware, AttachAware {
  static const String dragHintHoverClass = "dragHint-hover";
  Logger _logger = new Logger("jb_image_import.component");
  bool _attributesLoaded = false;
  bool _templateLoaded = false;

  Element _node;
  DivElement _imagePreviewContainer;
  ImageElement _preview;
  Element _jbFileImport;

  String _width;
  String _height;
  String _defaultColor;
  String _hoverColor;

  @NgTwoWay("src")
  String src;

  ///gives the raw file object to the outside world
  @NgTwoWay("raw-file")
  File rawFile;

  @NgOneWay("allowed-types")
  List<String> allowedTypeStrings = ["image/*"];

  List<File> _files;
  bool isDragOver = false;

  bool isHoverOverPreview = false;

  JbImageImport(this._node) {}

  get defaultColor => _defaultColor;

  @NgAttr('color')
  set defaultColor(String value) => _defaultColor = value ?? "rgba(100, 100, 100, 0.5)";

  get files => _files;

  set files(List<File> value) {
    if (value != null && value.length > 0 && value[0] != null) {
      _files = value;
      var file = value[0];

      rawFile = file;
      getImageFromFileAsDataUrl(file).then((image) => src = image);
    }
  }

  get height => _height;

  @NgAttr('height')
  set height(String value) => _height = value ?? "200px";

  get hoverColor => _hoverColor;

  @NgAttr('color-hover')
  set hoverColor(String value) => _hoverColor = value ?? "#373737";

  get imgAvailable => src != null && src.isNotEmpty;

  get width => _width;

  @NgAttr('width')
  set width(String value) => _width = value ?? "300px";

  @override
  void attach() {
    _attributesLoaded = true;
    onLoadComplete();
  }

  void onLoadComplete() {
    if (!(_attributesLoaded && _templateLoaded)) {
      return;
    }

    _imagePreviewContainer.style
      ..width = width
      ..height = height;

    document
      ..onDragEnter.listen((_) => isDragOver = true)
      ..onDragOver.listen((_) => isDragOver = true)
      ..onDrop.listen((_) => isDragOver = false)
      ..onDragLeave.listen((MouseEvent e) {
        e.preventDefault();
        Element target = e.target as Element;

        if (target != null && !_node.contains(target)) isDragOver = false;
      });

    _preview
      ..onMouseOver.listen((_) => isHoverOverPreview = true)
      ..onMouseOut.listen((_) => isHoverOverPreview = false);

    _jbFileImport
      ..onMouseOver.listen((_) => (imgAvailable) ? isHoverOverPreview = true : isHoverOverPreview = false)
      ..onMouseOut.listen((_) => isHoverOverPreview = false);
  }

  @override
  void onShadowRoot(ShadowRoot shadowRoot) {
    _imagePreviewContainer = shadowRoot.querySelector(".ImagePreviewContainer");
    _preview = shadowRoot.querySelector("#preview");
    _jbFileImport = shadowRoot.querySelector("jb-file-import");

    _templateLoaded = true;
    onLoadComplete();
  }
}
