library jb_image_import;

import 'package:angular/angular.dart';
import 'package:jb_file_import/jb_file_import.dart';
import 'src/component.dart';

// Export any libraries here which are intended for clients of this package.

export 'src/component.dart';

class JbImageImportModule extends Module {

  JbImageImportModule () {
    install(new JbFileImportModule());
    bind(JbImageImport);
  }
}