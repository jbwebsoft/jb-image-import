# Changelog

## 1.1.3
- refactored attributes to be NgAttr with null checks

## 1.1.2
- fixed allowed-types param implementation

## 1.1.1
- fixed src attribute

## 1.1.0
- added raw-file, color and color-hover attributes and updated readme

## 1.0.1
- changed preview opacity to 0.4

## 1.0.0
- Initial version
- Click to choose image - mode
- supports drag and drop of one image
