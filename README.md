# jb_image_import

A drag and drop image import component for various applications

## Installation
1. Add the component to your pubspec.yaml

    - Use git https version (better for public dependencies)
                
            dependencies:
              jb_image_import:
                git:
                  url: https://<URL einfügen>

    - Git with specific branch

            dependencies:
              jb_image_import:
                git:
                  url: https://<URL einfügen>
                  ref: some-branch | tag

2. Show your component to angular dependency injection -> see App Class in app_initialize.dart

        import 'package:jb_image_import/jb_image_import.dart';

        class App extends Angular.Module {

          App() {
            bind(JbImageImport);
          }
        }

## Tag Usage

    <jb-image-import> </jb-image-import>

### Attributes

- **width**
    - Type: css dimension string (e.g. 100% or 200px)
    
- **height**
    - Type: css dimension string (e.g. 100% or 200px)
    
- **color**
    - Sets border and font color in default state
    - Type: css color string
    
- **color-hover**
    - Sets border and font color when you drag over the component
    - Type: css color string

- **src**
    - Type: String 
    - could be normal path when displayed image comes from outside
    - is data-url when image comes from inside (chosen or per drag'n drop)
 
- **raw-file** 
    - Type: two way bound File object of inside image chosen or per drag'n drop

## Some further knowledge hints

- Obtaining injector:
        final injector = applicationFactory().addModule(new SpeedpadApp()).run();`
- Logger Configuration:
        //init logging
        hierarchicalLoggingEnabled = true;
        Logger.root.onRecord.listen(new LogPrintHandler());

        Logger.root.level = Level.OFF;
        _libLogger.level = Level.ALL;
        //could be customized with _libLogger.level =  Level.INFO or Level.OFF and
        // then add specific logger
        //_logger.level = Level.All

- DI register possibilities:
        // EXAMPLE: bind(StorageService);
        // EXAMPLE: bind(StorageService, toValue: new StorageService());
        // EXAMPLE: bind(StorageService, toFactory: (Angular.Injector inj) => new StorageService(inj.get(EventBus)));